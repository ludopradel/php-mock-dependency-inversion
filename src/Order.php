<?php

namespace craft;

class Order
{

    private $product;

    public function __construct($product) {
        $this->product = $product;
    }

    public function isValid() {
        return $this->product != "";
    }
        
}