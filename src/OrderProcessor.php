<?php

namespace craft;

class OrderProcessor
{

    public function __construct() {
        
    }

    public function process(Order $order)
    {
        $dbRepository = new DBRepository();
        if ($order->isValid() && $dbRepository->save(order))
        {
            $mailSender = new MailSender();
            $mailSender->sendConfirmationMessage(order);
        }

    }
}