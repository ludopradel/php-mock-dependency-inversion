<?php

namespace craft;

class DBRepository implements IRepository
{
    public function save(Order $order)
    {
        throw new \Exception("Don't call me directly, use a double instead !");
    }
}