<?php

namespace craft;

interface ISender
{
    public function SendConfirmationMessage(Order $order);
}