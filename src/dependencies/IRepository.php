<?php

namespace craft;

interface IRepository
{
    public function save(Order $order): bool;
}

